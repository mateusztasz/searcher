package com.mateusz.searcher.starter;

import com.mateusz.searcher.httpApi.HttpServerVerticle;
import com.mateusz.searcher.olx.OlxWebClientVerticle;
import io.vertx.core.AbstractVerticle;

public class VertxStarterVerticle extends AbstractVerticle {

    @Override
    public void start() {
        vertx.deployVerticle(new HttpServerVerticle());
        vertx.deployVerticle(new OlxWebClientVerticle());
    }
}
