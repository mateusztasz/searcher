package com.mateusz.searcher.httpApi;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import static com.mateusz.searcher.olx.OlxWebClientVerticle.OLX_VERTICLE_EVENT_ADDRESS;

public class HttpServerVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> promise) {
        final Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.get("/offer/olx/:product").handler(this::getProductHandler);

        vertx.createHttpServer().requestHandler(router).listen(8080, result -> {
            if (result.succeeded()) {
                promise.complete();
            } else {
                promise.fail(result.cause());
            }
        });
    }

    private void getProductHandler(RoutingContext routingContext) {
        final String message = routingContext.request().getParam("product");

        vertx.eventBus().request(OLX_VERTICLE_EVENT_ADDRESS, message, reply -> {
            if (reply.succeeded()) {
                routingContext.response()
                        .putHeader("content-type", "application/json")
                        .end((String) reply.result().body());
            } else {
                routingContext.response().setStatusCode(500).setStatusMessage("Something went wrong");
            }
        });

    }

}

