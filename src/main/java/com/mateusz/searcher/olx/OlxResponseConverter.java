package com.mateusz.searcher.olx;

import com.mateusz.searcher.olx.model.Product;
import com.mateusz.searcher.olx.model.ProductList;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class OlxResponseConverter {
    public static final Logger LOGGER = LoggerFactory.getLogger(OlxResponseConverter.class);

    public ProductList retrieveProductsFrom(String htmlResponse) {
        List<Product> result = new ArrayList<>();
        Document doc = Jsoup.parse(htmlResponse);

        for (Element element : doc.getElementsByAttributeValue("summary", "Ogłoszenie")) {
            ElementHandler elementHandler = new ElementHandler(element);
            String id = elementHandler.extractData(xe -> xe.attr("data-id"));
            String title = elementHandler.extractData(xe -> xe.select(".title-cell").select("strong").get(0).childNodes().get(0).outerHtml());
            String price = elementHandler.extractData(xe -> xe.select(".td-price").select("strong").get(0).childNodes().get(0).outerHtml());

            result.add(new Product(id, title, price));

        }
        return new ProductList(result);
    }

    static class ElementHandler {
        private final Element element;

        public ElementHandler(final Element element) {
            this.element = element;
        }

        public String extractData(final Function<Element, String> function) {
            try {
                return function.apply(element);
            } catch (Exception ex) {
                LOGGER.debug(String.format("Could not parse html element.: %s", element.outerHtml()));
                return "unknown";
            }
        }
    }
}
