package com.mateusz.searcher.olx.model;

import java.util.Collections;
import java.util.List;

public class ProductList {
    public final List<Product> offers;

    public ProductList(List<Product> offers) {
        this.offers = Collections.unmodifiableList(offers);
    }
}
