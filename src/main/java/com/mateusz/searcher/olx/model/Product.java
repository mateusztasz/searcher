package com.mateusz.searcher.olx.model;

public class Product {
    public final String id;
    public final String name;
    public final String price;

    public Product(String id, String name, String price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
}
