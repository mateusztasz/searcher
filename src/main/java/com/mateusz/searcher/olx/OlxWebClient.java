package com.mateusz.searcher.olx;

import com.mateusz.searcher.olx.model.ProductList;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.client.WebClient;

class OlxWebClient implements Handler<Message<String>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OlxWebClient.class);

    private final WebClient webClient;
    private final OlxResponseConverter olxResponseConverter;

    OlxWebClient(final WebClient webClient, final OlxResponseConverter olxResponseConverter) {
        this.webClient = webClient;
        this.olxResponseConverter = olxResponseConverter;
    }

    @Override
    public void handle(Message<String> message) {
        String receivedMsg = message.body();
        LOGGER.info("Received message from event bus: " + receivedMsg);

        // Session web client could not handle properly automatic redirection even though setFollowRedirection
        // was set true.
        //TODO Request a form POST and handle redirection
        webClient
                .get(443, "olx.pl", uriFor(receivedMsg))
                .ssl(true)
                .send(httpResponseAsyncResult -> {
                    if (httpResponseAsyncResult.succeeded()) {
                        String htmlResponse = httpResponseAsyncResult.result().bodyAsString();
                        ProductList result = olxResponseConverter.retrieveProductsFrom(htmlResponse);

                        message.reply(Json.encodePrettily(result));
                    } else {
                        message.reply(Json.encodePrettily("Error occurred during fetching data from olx."));
                        LOGGER.error("Error occurred during fetching data from olx.");
                    }
                });

    }

    private String uriFor(String receivedMsg) {
        return String.format("/oferty/q-%s", receivedMsg.trim().replaceAll(" ", "-"));
    }

}
