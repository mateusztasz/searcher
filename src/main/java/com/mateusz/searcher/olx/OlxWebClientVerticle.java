package com.mateusz.searcher.olx;

import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.client.WebClient;

public class OlxWebClientVerticle extends AbstractVerticle {

    public static final String OLX_VERTICLE_EVENT_ADDRESS = "address";

    @Override
    public void start() {
        WebClient webClient = WebClient.create(vertx);
        vertx.eventBus().consumer(OLX_VERTICLE_EVENT_ADDRESS, new OlxWebClient(webClient, new OlxResponseConverter()));
    }

}


