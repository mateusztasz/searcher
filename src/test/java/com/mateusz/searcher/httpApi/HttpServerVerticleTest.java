package com.mateusz.searcher.httpApi;

import com.mateusz.searcher.olx.model.Product;
import com.mateusz.searcher.olx.model.ProductList;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(VertxUnitRunner.class)
public class HttpServerVerticleTest {
    private Vertx vertx;

    private int port = 8080;

    @BeforeClass
    public static void beforeClass() {

    }

    @Before
    public void setup(TestContext testContext) {
        vertx = Vertx.vertx();

        vertx.deployVerticle(HttpServerVerticle.class.getName(), testContext.asyncAssertSuccess());
    }

    @After
    public void tearDown(TestContext testContext) {
        vertx.close(testContext.asyncAssertSuccess());
    }

    @Test
    public void shouldExecuteGetRequest_andReturnValueFromEventBus(TestContext testContext) {
        final Async async = testContext.async();

        ArrayList<Product> products = new ArrayList<>();
        products.add(new Product("1", "car1", "150"));
        products.add(new Product("2", "car2", "250"));
        ProductList obj = new ProductList(products);

        vertx.eventBus().consumer("address", h -> {
            h.reply(Json.encodePrettily(obj));
        });

        vertx.createHttpClient()
                .getNow(port, "localhost", "/offer/olx/car", response -> {
                    response.exceptionHandler(testContext.exceptionHandler());
                    response.handler(responseBody -> {
                        assertThat(responseBody.toString()).isEqualTo("{\n" +
                                "  \"offers\" : [ {\n" +
                                "    \"id\" : \"1\",\n" +
                                "    \"name\" : \"car1\",\n" +
                                "    \"price\" : \"150\"\n" +
                                "  }, {\n" +
                                "    \"id\" : \"2\",\n" +
                                "    \"name\" : \"car2\",\n" +
                                "    \"price\" : \"250\"\n" +
                                "  } ]\n" +
                                "}");
                        async.complete();
                    });
                });
    }

}
